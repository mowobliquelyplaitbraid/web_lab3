package main.controller;

import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("ControlValidator")
public class ControlValidator implements Validator {
    private static final double minX = -3;
    private static final double maxX = 5;

    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        try {
            Double x = Double.parseDouble(o.toString().replace(',', '.'));
            System.out.println(o.toString());
            System.out.println(x);
            if (!(x >= minX && x <= maxX) || (!o.toString().equals("0.0") && (x == 0.0))) {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            FacesMessage msg =
                    new FacesMessage("Y validation failed.",
                            "pupupepe Y."
                    );
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().showMessageInDialog(msg);
            throw new ValidatorException(msg);
        }
    }
}

