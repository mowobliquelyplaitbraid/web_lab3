package main.util;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class HibernateSession {
    @Produces
    @PersistenceContext(unitName = "hibernate")
    private EntityManager entityManager;
}
